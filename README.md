Instructions

1. Clone this repo
2. Install Julia https://julialang.org/downloads/
3. Go to cloned repo directory
4. Run `script1.jl` with the command `julia --project=. -e 'using Pkg; Pkg.resolve(); Pkg.instantiate(); include("script1.jl");'`
5. The figures should be generated
