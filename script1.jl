using FFTW
using Plots
using DSP
using DSP.Windows
using Statistics
using Optim
using Roots
using LaTeXStrings

M = 17
N = 1024
K = 5
σ = 1.0
ϕ0 = π/2
Ts = 1e-5

ϕ1 = π/4

f0s = (range(-0.5, stop=0.5, length=100) .+ M)/(N*Ts)

ϕ1s_mle = similar(f0s)
ϕ1s_fft = Dict(rect => similar(f0s), hanning => similar(f0s))

A(ϕ) = exp.(0.5im*[1;-1]*ϕ)
∇A(ϕ) = 0.5im*[1;-1] .* A(ϕ)

for (i, f0) = enumerate(f0s)
    ns = 0:N-1
    ss = @.( exp((2π*f0*ns*Ts + ϕ0)im) ) #Source signal

    rs = A(ϕ1)*transpose(ss) + σ * randn(2, N) #Received signals

    # MLE
    #Likelihood function (without the constant terms)
    L(ϕ) = sum( (rn - A(ϕ)*sn)'*(rn - A(ϕ)*sn) |> real for (rn, sn) in zip(eachcol(rs), ss) ) / σ^2
    #Gradient of Likelihood (note that |s|Aᴴ(ϕ)A(ϕ) = |s|c, so this term vanishes in gradient)
    ∇L(ϕ) = 2sum( real(rn'*∇A(ϕ)*sn) for (rn, sn) in zip(eachcol(rs), ss) ) 

    # Solve
    # res = optimize(L, -2π, 2π)
    # ϕ1_mle = res.minimizer
    ϕ1_mle = find_zero(∇L, 0.0)
    ϕ1s_mle[i] = ϕ1_mle

    # FFT
    for (wind, ϕ1s_w) in ϕ1s_fft
        if wind === tukey
            w = wind(N, α = 0.2)
        else
            w = wind(N)
        end
        xs = [r .* w for r in eachrow(rs)]

        Ys = fft.(xs)
        _, k0 = Ys[1][1:512] .|> abs |> findmax

        ϕ1_fft = angle(Ys[2][k0-K:k0+K]'*Ys[1][k0-K:k0+K])
        ϕ1s_w[i] = ϕ1_fft
    end
end

println("MLE Var(ϕ1) = $(var(ϕ1s_mle))")
for (k, v) in ϕ1s_fft
    println("FFT $k window Var(ϕ1) = $(var(v))")
end

fig1 = plot(f0s, ϕ1s_mle, label="MLE")
for (k, v) in ϕ1s_fft
    plot!(fig1, f0s, v, label="FFT $k")
end
plot!(fig1, f0s[[1,end]], [ϕ1, ϕ1], label="Expected")
xlabel!(fig1, L"$f_0$")
ylabel!(fig1, L"$\hat{\phi}_1$")

savefig(fig1, "f0_vs_phi1.svg")